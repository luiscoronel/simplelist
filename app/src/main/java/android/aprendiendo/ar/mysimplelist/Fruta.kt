package android.aprendiendo.ar.mysimplelist

import android.os.Parcel
import android.os.Parcelable

class Fruta(name: String?,image: Int): Parcelable {

    //variable que se lee en el constructor
    var nombre: String? = ""
    var imagen:Int = 0

    init {
        this.nombre = name
        this.imagen = image
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeString(nombre)
        parcel.writeInt(imagen)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Fruta> {
        override fun createFromParcel(parcel: Parcel): Fruta {
            return Fruta(parcel)
        }

        override fun newArray(size: Int): Array<Fruta?> {
            return arrayOfNulls(size)
        }
    }

    constructor(parcel: Parcel) : this(
        parcel.readString(),
        parcel.readInt()
    )
}