package android.aprendiendo.ar.mysimplelist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.AdapterView
import android.widget.ListView
import android.widget.Toast
//import java.util.ArrayList
import kotlin.collections.ArrayList

//import kotlin.collections.ArrayList

class Main2Activity : AppCompatActivity() {

    //val intent = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main2)




        //val intent = intent.extras
        //val i = Intent()
        //val list: ArrayList<Fruta> = intent!!.getParcelableArrayList("TAG")
        var list:ArrayList<Fruta> = intent.getParcelableArrayListExtra<Fruta>("TAG") as ArrayList<Fruta>
           //list.addAll(intent!!.getParcelableArrayList("TAG"))
        val lista: ListView = findViewById(R.id.lista)

        //val adaptador = ArrayAdapter(this, android.R.layout.simple_list_item_1, l)
        val adaptador = AdaptadorCustom(this,  list)
        lista.adapter = adaptador

        lista.onItemClickListener = AdapterView.OnItemClickListener { parent,
                                                                      view,
                                                                      position,
                                                                      id ->

            //var ID: Long = lista.id as Long

            if(list.get(position).nombre == null){
                Toast.makeText(this, "Elemento no encontrado", Toast.LENGTH_SHORT).show()
            }else{
                Toast.makeText(this, "ID: " + list.get(position).nombre, Toast.LENGTH_SHORT).show()
            }



        }
    }
}
