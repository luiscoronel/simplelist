package android.aprendiendo.ar.mysimplelist

interface Alimento {

    fun comer()

    fun postre()

    fun ensalada()

    fun segundoPlato()
}