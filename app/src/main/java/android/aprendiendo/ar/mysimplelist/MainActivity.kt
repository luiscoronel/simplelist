package android.aprendiendo.ar.mysimplelist

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.ContextMenu
import android.view.View
import android.widget.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val buton:Button = findViewById(R.id.button)

        val l:ArrayList<Fruta> = ArrayList()

        l.add(Fruta("Manzana", R.drawable.manzana))
        l.add(Fruta("Sandia", R.drawable.sandia))
        l.add(Fruta("Platano", R.drawable.platano))
        l.add(Fruta("Durazno", R.drawable.durazno))
        l.add(Fruta("Sandia", R.drawable.sandia))
        l.add(Fruta("Manzana", R.drawable.manzana))


        buton.setOnClickListener{
            val intent = Intent(this, Main2Activity::class.java)
            intent.putParcelableArrayListExtra("TAG", l)
            startActivity(intent)
        }
    }

    /**
     *
     * Agregando un commit no deseado
     * para prueba
     *
     * */

}
